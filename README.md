<p align="center"><img src="cover.png" ></p>

<p align="center">

</p>

## About Product Photo Gallery

Product Photo Gallery is a web-based software. The proto of products are fetched from external API using Javascript & jQuery. Though, this apps been created with Laravel framework, to communicate with API , I used JS for frontend cached storage, due to frontend cached storage, Once a product is loaded on page, to retrive it again , no need to call API again. 


### How can used?
Got to domain/server. At first need to login. By default, user access are already given.
Go to home gpage, Click [more see](https://prnt.sc/sea22F1rlEfZ) button to load data. For every click, it will load 4 products. To see datails of any single product click on [photo](https://prnt.sc/Rrdyz9fZl-z5) . At single product page, You can change product [modifications & items](https://prnt.sc/NxUfD8PymwPO) . There are two type of product modifications such as White & Black and  three types of items such as small, medium & large. 

### How product modification works internally.
Two type of modifications - <b>White</b> & <b>Black color</b>. Every odd product id act as [black color](https://prnt.sc/jQ9CKjga70Ax) modification & even product id act as White color modification. 

### Product items :
There are three types of product item such as small, medium & large. If you click [medium item](https://prnt.sc/EJ-1u-9Nx-uz) button, it will fetched [medium images](https://prnt.sc/EpwiU4fIfH-J) of this product modifications.

You may change [items parameter](https://prnt.sc/w3XVnF9ZvoMf) value from URL manually.
If items value is less than 600, it act as small item, if value is less than 1800, it will act as medium item & if item greater than 1800, it will be large item size.

### Installation :
To run this project in local server / live server, there is no need to configure anything. Just install composer on the project directory. [Hints](https://medium.com/@colorfield/install-an-existing-laravel-project-c6e6bf28d5c6)
No need database connection as data are fetched from API.

### Limitation :
As browser localstorage is used as frontend cached memory, so maximum 10MB space are allowed to handle data. 
To get more scalability & low latency, you may use Redis as backend cached memory.

### Technologies :
[Laravel 8](https://laravel.com/docs/8.x/installation), Javascript, jQuery, [Fancybox](http://fancybox.net/), [OwlCarousel](https://owlcarousel2.github.io/OwlCarousel2/)

