<!-- fancybox css -->
<link rel="stylesheet" href="{{ asset('js/fancybox/jquery.fancybox-1.3.4.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
<link href="http://fonts.cdnfonts.com/css/montserrat" rel="stylesheet">
<style type="text/css">
	body{
		margin: 0px;
		padding: 0px;
		font-family: Montserrat;
		font-style: normal;
	}
	.wrapper{
		max-width: 1120px;
		margin: auto;
		background: #f1f0f0;
		padding: 0px 10px;
	}
	.navbar {
	  background-color: #333;
	  overflow: hidden;
	}

	/* Style the links inside the navigation bar */
	.navbar a {
	  float: left;
	  color: #f2f2f2;
	  text-align: center;
	  padding: 14px 16px;
	  text-decoration: none;
	  font-size: 17px;
	}

	/* Change the color of links on hover */
	.navbar a:hover {
	  background-color: #ddd;
	  color: black;
	}

	/* Add a color to the active/current link */
	.navbar a.active {
	  background-color: #04AA6D;
	  color: white;
	}
	/******Reload Wrapper******/
	.reload_wrapper{
		position: fixed;
	    width: 100%;
	    height: 100%;
	    z-index: 99999999;
	    text-align: center;
	    top: 0px;
	    background: white;
	    left: 0px;
	    opacity: .9;
	}
	.reload_wrapper .loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid blue;
		  border-right: 16px solid green;
		  border-bottom: 16px solid red;
		  border-left: 16px solid pink;
		  width: 120px;
		  height: 120px;
		  -webkit-animation: spin 2s linear infinite;
		  animation: spin 2s linear infinite;
		  margin: auto;
		  margin-top:  5%;
	}

		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
</style>