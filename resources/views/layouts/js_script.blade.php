<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!--
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
-->
<script src="{{ asset('js/fancybox/jquery.fancybox-1.3.4.js') }}"></script>
<script src="{{ asset('js/fancybox/jquery.easing-1.3.pack.js') }}"></script>
<script src="{{ asset('js/fancybox/jquery.mousewheel-3.0.4.pack.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        // Pre-loader delay to execute 2 sec 
        setTimeout(function() { 
            $(".reload_wrapper").css("display", "none");
         }, 2000);
        
        //Init Fancybox
        $("a#single_image").fancybox();

        // Init Owl Carousel
        $('.owl-carousel').owlCarousel({
            center: false,
            items:4,
            loop:false,
            margin:10,
            responsive:{
                600:{
                    items:4
                }
            }
        });
        
        //Get product details by id/modification/item from API or frontend cached data 
        const get_data = async function(product_id = 0){
            let products_table = JSON.parse(get('products_table'));
            let result = '';
            // If product exists on local memory, then return data
            if(products_table && products_table[product_id] ){
                console.log('Getting from Local')
                return products_table[product_id] ;
            }else{
                // _api_state_ is 0 or undefined means - Unauthorized state or something is wrong
                if( get('_api_state_') == 0 || get('_api_state_')  == undefined){
                    alert('Unauthorized. Kindly Login.');
                    return
                }
                let _url = "https://gallery-api.engine.lt/api/gallery/"+product_id ;
                let _token = get('_auth_token_') ;
                $.ajax({
                    type: 'GET',
                    url: _url,
                    async: false, 
                    headers: {
                        Authorization: 'Bearer '+_token ,
                    },
                    success : function(data) {
                        if(data && data['id']){
                            console.log('Getting from API')
                            add_product(data);
                            result = data;
                            store('_api_state_', 1);
                        }
                   },
                   error: function(err) {
                        switch (err.status) {
                            case 400:
                                alert('Bad Request');
                                store('_api_state_', 0);
                            break;
                            case 401:
                                alert('Unauthorized. Kindly Login.');
                                store('_api_state_', 0);
                            break;
                            case 403:
                                alert("Forbidden");
                                store('_api_state_', 0);
                            break;
                            default:
                                alert('Something is wrong.');
                                store('_api_state_', 0);
                            break;
                            }
                        },
                }); 
                return result;
            }
            
        }

        //  Store product details into local storage that act as frontend cached memory
        function add_product(data = ''){
            if(data && data['id']){
                var product_id = data['id'] ;
                var products_table = '';
                if(get('products_table')){
                    products_table = JSON.parse(get('products_table'));

                    // If already exists product, then return, no need to add again
                    if(products_table[product_id]){
                        //console.log('Product : '+ product_id + " already exists.");
                        return ;
                    }else{
                        products_table[product_id] = data;
                    }
                }else{
                    products_table = {};
                    products_table[product_id] = data;
                }
                if(products_table){
                    store('products_table', JSON.stringify(products_table));
                }
            }
            
        }

        // Create dynamic carousel
        function create_owl_carousel(data, item=400){
            let size = 0;
            if(item <= 75){
                size = 0
            } else if( item <= 640){
                size = 1
            }else{
                size = 2
            }
            let images = data['dimensions'][size]['images'];
            $("#single_image").attr("href", images[0] )
            $("#single_image").find('img').attr("src", images[0] )
            let container = '';
            for(let i in images){
                let img = $(".items_carousel").find(".item:eq("+i+")").find('img') ;
                if(img.attr('src') != undefined){
                    img.attr('src', images[i])
                }
            }

        }

        // Call this function whenever page loading done or you may call anytime
        const autoload = async function autoload(item = 0){
            if( get('_api_state_') == 0 || get('_api_state_')  == undefined){
                alert('Unauthorized. Kindly Login.');
                if(Number(window.location.pathname.replaceAll("/", "").trim())){
                    document.location.href="/";
                }else{
                    return
                }
            }
            let _path = window.location.pathname;
            let product_id = Number(_path.replaceAll("/", "").trim());
            let params = (new URL(document.location)).searchParams;
            let _item = Number( params.get("items") );
            let _modifications = Number(params.get("modifications"));
            if(_modifications && !isNaN(_modifications)){
                product_id = _modifications ;
            }
            if(_item && !isNaN(_item)){
                item = _item ;
            }
            changeItemButton(item);
            changeModificationBtn(_modifications);
            if(!isNaN(product_id)){
                try {
                    const data = await get_data(product_id) ;
                    //console.log('Data:', data);
                    create_owl_carousel(data, item);
                } catch (error) {
                    console.log('Error:', error);
                }
            }
            
        }
        // Change path of URL without reloading page
        // Trigger whenever changed item/modification  https://prnt.sc/KlrPBx_uOeTD
        function changePath(type=null, id = null){
            if(type && id){
                let win = window.location;
                let _domain = win.origin; //  http://127.0.0.1:8000
                let _path = win.pathname; //  /products/4
                let _filter = win.search.replaceAll('?', '');   //    ?item=99 & modification=94
                let _final_result = '';
                let _full_url = '';
                if(_filter.length && _filter.includes('&')){
                    let arr = _filter.split('&');
                    for(let i in arr){
                        let temp = arr[i].trim().toLowerCase() ;
                        if(temp.length && temp.includes(type) && temp.includes('=')){
                            let temp_item = temp.split("=") ;
                            if(temp_item[0] == type){
                                _final_result += type + "="+ id+"&" ;
                            }else{
                                _final_result += temp+"&" ;
                            }
                            
                        }else{
                             _final_result += temp+"&";
                        }
                    }
                }else{
                    if(_filter.length >= 3){
                        _final_result +='&'+ type+ "="+id ;
                    }else{
                        _final_result += type+ "="+id ;
                    }
                    
                }
                if(_final_result.slice(-1) == "&"){
                    _final_result = _final_result.substring(0, _final_result.length - 1);
                }
                if(_final_result && _final_result.slice(0,1) == "&"){
                    _final_result = _final_result.substring(1) ;
                }
                if(_final_result){
                    _final_result = '?'+_final_result;
                }
                _full_url = _domain + _path + _final_result;
                window.history.pushState({}, '', _full_url); 
            }
        }

        // Change Item button style dynamically
        function changeItemButton(value){
            if(value < 600){
                value = 1
            }else if(value < 1800){
                value = 600
            }else{
                value = 1800
            }
            let btn = $(".product_item").find("[value='" + value + "']"); 
            $(".item-btn").removeClass('active_btn');
            btn.addClass("active_btn");
            
        }

        // Change Modification button style dynamically
        function changeModificationBtn(id=0) {
            let btn = $(".product_modification"); 
            if(! isNaN(Number(id)) && id > 0){
                // If modification id is odd number, then mark it as Black modification else White 
                $(".modification-btn").removeClass('active_btn');
                if(id%2){
                    btn.find("[value='black']").addClass('active_btn');
                }else{
                    btn.find("[value='white']").addClass('active_btn');
                }
            }
        }

        // Get images from local cached memory as array
        const get_images = async function(){
            let products_table = JSON.parse(get('products_table'));
            let images = [];
            $.each(products_table, function(key,val) { 
                var img = '';
                try{
                    img = [key, val['dimensions'][1]['images'][0]] ;        
                } catch{

                } 
                if(img){
                    images.push(img)
                }    
            });   
            return images ;
        }

        // Create product gallery dynamically for home page  https://prnt.sc/RZKPO9dtlN3C
        function create_gallery(images) {
            let div = '';
            let url = window.location.origin ;
            for(let i in images){
                div += "<div class='single_img'><a href='"+url+"/"+ images[i][0]+ "'><img src='"+ images[i][1]+"' ></a> <br><span> Product ID : "+images[i][0] +"</span> </div>"
            }
            $(".product_gallery").append(div);
        }

        // This async function will execute after load completing to create product gallery   
        const run = async function(){
            let images = await get_images() ;
            create_gallery(images);
        }
      
        // Get more products from API
        const get_more = async function(max_number = 2){
            let count = 0;
            let generate_id = 1;
            let images = {};
            while(count < max_number){

                if( get('_api_state_') == 0 || get('_api_state_')  == undefined){
                    alert('Unauthorized. Kindly Login.');
                    return
                }
                products_table = JSON.parse(get('products_table'));
                if( (products_table && ! products_table[generate_id]) || !products_table){
                    const data = await get_data(generate_id) ;
                    if(data && data['id']){
                        images[generate_id] = data ;
                  
                        var img = '';
                        try{
                            img = [[generate_id, data['dimensions'][1]['images'][0]]] ; 
                            create_gallery(img) ;  
                            count += 1;     
                        } catch{
                        } 
                    }
                }
                generate_id += 1;
                if(generate_id > 1000){
                    return
                }
            }
        }

        // Click more_see button to load more product on home page https://prnt.sc/C89TahAnXOzl
        $(".more_see").click(function(){
            if( get('_api_state_') == 0 || get('_api_state_')  == undefined){
                alert('Unauthorized. Kindly Login.');
                return
            }
            let btn = $(this);
            $(".reload_wrapper").removeAttr("style");
            btn.attr("disabled", true) 
            get_more(4).then(function(){
                btn.removeAttr('disabled');
                $(".reload_wrapper").css("display", "none");
                console.log("ooo")
            })
            
        })

        // Execute it after load completing
        autoload();
        run();

        // Submit event for login form  https://prnt.sc/bdNR9gBrCzCa
        $("#login_form").submit(function(){
            let _url = "https://gallery-api.engine.lt/api/auth/login" ;
            $("#login_form").find(':submit').attr('disabled','disabled');
                $.ajax({
                    type: 'POST',
                    url: _url,
                    async: false, 
                    data :JSON.stringify({
                        'email' : $("#login_email").val().trim(),  //'tested276by@gmail.com',
                        'password' : $("#login_password").val().trim(), //'12160406nF'
                    }),
                    dataType  : 'json',
                    contentType: "application/json; charset=utf-8",
                    success : function(data) {
                        if(data && data['error']){
                            alert('Email or Password is Wrong');
                            store('_api_state_', 0);
                        }else{
                            store('_auth_token_', data['access_token'] );
                            store('_api_state_', 1);
                            alert("Login successfully.")
                            location.reload();
                            //console.log(data)
                        }
                   },
                   error: function(err) {
                        switch (err.status) {
                            case 400:
                                alert('Bad Request');
                                store('_api_state_', 0);
                            break;
                            case 401:
                                alert('Unauthorized. Kindly Login.');
                                store('_api_state_', 0);
                            break;
                            case 403:
                                alert("Forbidden");
                                store('_api_state_', 0);
                            break;
                            default:
                                alert('Something is wrong.');
                                store('_api_state_', 0);
                            break;
                            }
                        },
                }); 
            $("#login_form").find(':submit').removeAttr('disabled');
            return false;
        })        

        // Logout Trigger
        $(".logout_link").click(function(){
            let _url = "https://gallery-api.engine.lt/api/auth/refresh" ;
            let _token = get('_auth_token_') ;
            $.ajax({
                type: 'POST',
                url: _url,
                async: false, 
                headers: {
                        Authorization: 'Bearer '+_token ,
                    },
                dataType  : 'json',
                contentType: "application/json; charset=utf-8",
                success : function(data) {
                    if(data && data['error']){
                        alert('Something is wrong!');
                    }else{
                        remove('_auth_token_');
                        alert("Logout successfully.");
                        store('_api_state_', 0);
                        location.reload();
                    }
               },
               error: function(err) {
                    switch (err.status) {
                        case 400:
                            alert('Bad Request');
                        break;
                        case 401:
                            alert('Unauthorized. Kindly Login.');
                        break;
                        case 403:
                            alert("Forbidden")
                        break;
                        default:
                            alert('Something is wrong.');
                        break;
                        }
                    },
            }); 
        })

        // Click event for modification button   https://prnt.sc/vi6vejIt-FB5 
        $(".modification-btn").click(function(){
            if($(this).hasClass("active_btn")){
                return ;
            }
            let btn_val = $(this).attr("value") ;
            let _path = window.location.pathname;
            let product_id = Number(_path.replaceAll("/", "").trim());
            let params = (new URL(document.location)).searchParams;
            let _modifications = Number(params.get("modifications"));
            if(_modifications && !isNaN(_modifications)){
                product_id = _modifications ;
            }
            if(btn_val == "black"){
                if(product_id%2){
                    product_id += 2 ;
                }else{
                    product_id += 1
                }
            }else{
                if(product_id%2){
                    product_id += 1 ;
                }else{
                    product_id += 2
                }
            }
            changePath('modifications', product_id);
            changeModificationBtn(product_id);
            autoload(product_id) 
        })

        // Click on item button to change item such as small, medium or large
        $(".item-btn").click(function(){
            if($(this).hasClass("active_btn")){
                return ;
            }
            let val = Number($(this).attr("value")) ;
            changePath('items', val);
            autoload(val) ;
            
        });

        //Click on item, It will set on Top  https://prnt.sc/fYLAMPOmM_rc
        $(".owl-carousel .item").click(function(){
            let get_img = $(this).find('img').attr("src") ;
            if(get_img != undefined){
                $(".product_image a").attr("href", get_img);
                $(".product_image a img").attr("src", get_img);
            }
        });
        
        
    })

</script>