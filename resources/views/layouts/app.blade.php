<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="/uploads/favicon.png"/>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>
        @include('layouts.style')

    <head>
    <body>
        <div class="wrapper">
            <div class="nav_container">
                <div class="navbar">
                  <a class="active" href="/">Home</a>
                  <a  class="login_link_btn" href="#">Login</a>
                  <a class="logout_link" href="#">Logout</a>
                </div>
            </div>
            <div class="content-wrapper">
                @yield('content')
            </div>
            <div class="reload_wrapper">
                <div class="loader"></div>
            </div>
        </div>
        
        @include('layouts.js_script')
        @yield('javascript')
        @include('layouts.login-modal')
    </body>
</html>