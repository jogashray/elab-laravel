@extends('layouts.app')
@section('title', 'Single Product: Elab Gallery')
<style type="text/css">
    .product_details{
        display: block;
        width: 100%;
    }
    .product_image{
        width: 100%;
        padding: 10px 0px;
        text-align: center;
    }
    .product_image img{
        max-width: 90%;
        min-width: 250px;
    }
    .product_description{
        width: 100%;
        margin-top: auto;
        text-align: center;
    }
    .item_images{
        tmext-align: center;
    }
    .item_images img{
        width: 150px;
        height: 150px;;
    }
    .owl-carousel .owl-item img {
        display: block;
        height: 150px;
        width: 200px;
    }
    .owl-carousel{
        margin-top: 30px;
    }
    .product_modification{
        display: inline-block;
        margin: 10px 0px;
        text-align: center;
        margin-top: 0px;

    }
    .product_description p{
        margin-bottom: 0px;
    }
    .product_modification .modification-btn{
        padding: 5px 14px;
        margin: 10px 5px;
        border: 1px solid #057e52;
        border-radius: 100%;
        cursor: pointer;
        color: white;
        width: 65px;
        height: 65px;
        background: #04aa6d;
    }
    .product_modification .modification-btn:hover{
        opacity: .9;
    }
    .product_modification .active_btn{
        font-size: 76%;
        font-weight: 800;
        background: #068757;
        color: wheat;
        border: 1px solid #025235;
        cursor: not-allowed;
    }
    .product_item{
        display: inline-block;
        margin: 10px;
    }
    .product_item .item-btn{
        padding: 5px 14px;
        margin: 0px 3px;
        border: 1px solid #dfd0d0;
        border-radius: 5px;
        cursor: pointer;
    }
    .product_item .item-btn:hover{
        background-color: white;
    }
    .product_item .active_btn{
            background: gray;
            cursor: auto;
            color: white;
            border: 1px solid #403e3e;
    }
    .product_item .active_btn:hover{
        background-color: gray;
        cursor: auto;
    }
    @media only screen and (min-width: 768px) {
        .product_details{
            display: flex;
            flex-direction: row;
        }
        .product_image{
            width: 50%;
            padding: 10px;
        }
        .product_image img{
            max-width: 90%;
        }
        .product_description{
            width: 50%;
            margin-top: auto;
        }

        .product_modification{
            display: flex;
            flex-direction: row;
            margin: 10px;
        }
        .product_modification .modification-btn{
            padding: 5px 14px;
            margin: 10px 5px;
        }
        .product_description{
            width: 100%;
            margin-top: auto;
            text-align: left;
        }
        .product_item{
            display: flex;
            flex-direction: row;
            margin: 10px;
        }
    }
</style>
@section('content')

<div class="product_container">
    <div class="product_details">
        <div class="product_image">
            <a id="single_image" href="www.dummy.com/example.jpg"><img src="www.dummy.com/example.jpg" alt=""/></a>
        </div>
        <div class="product_description">
            <div class="product_title">
                
            </div>
            <p>Product Modifications -</p>
            <div class="product_modification">
                <button class="modification-btn" value="white">White</button>
                <button class="modification-btn" value="black">Black</button>
            </div>
            <p>Product Items -</p>
            <div class="product_item">
                <button class="item-btn" value="1"> Small </button>
                <button class="item-btn" value="600"> Medium </button>
                <button class="item-btn" value="1800"> Large </button>
            </div>
        </div>
    </div>
    <div class="items_carousel owl-carousel owl-theme">
        <div class="item">
            <img src="#">
        </div>
        <div class="item">
            <img src="#">
        </div>
        <div class="item">
            <img src="#">
        </div>
        <div class="item">
            <img src="#">
        </div>
        <div class="item">
            <img src="#">
        </div>
    </div>
    
</div>


@endsection

@section('javascript')

@endsection