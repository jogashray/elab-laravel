@extends('layouts.app')
@section('title', 'Elab Gallery - Home')
<style type="text/css">
    .product_gallery{
        width: 100%;
        text-align: center;
    }
    .product_gallery .single_img{
        width: 100%;
        padding: 5px 0px;
        display: inline-block;
    }
    .product_gallery .single_img img{
        width: 100%;
        height: 250px;
    }
    .product_gallery img:hover {
        opacity: .9;
        transform: scale(1.03);
    }
    .footer{
        width: 100%;
        padding: 20px 0px;
        text-align: center;
    }
    .footer .more_see{
        border: 1px solid #005b7e;
        background: #077291;
        width: 100px;
        padding: 9px;
        font-size: 16px;
        border-radius: 5px;
        cursor: pointer;
        color: white;
    }
    .footer .more_see:hover{
        opacity: .8;
        font-size: 15px;
    }
     @media only screen and (min-width: 768px) {
        .product_gallery .single_img{
            width: 20%;
            padding: 5px;
            display: inline-block;
        }
     }
</style>
@section('content')

    <div class="product_gallery">
        
    </div>
    <div class="footer">
        <button class="more_see">More See</button>      
    </div>

@endsection

