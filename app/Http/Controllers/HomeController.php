<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
class HomeController extends Controller
{

    public function __construct(){    }

    public function index(Request $request, $id=null)
    {
        if(is_numeric($id)){
            return  view('gallery.single_product') ;
        }else{
            return  view('gallery.index') ;
        }
        
    }
}
